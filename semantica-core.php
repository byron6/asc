<?php

/**
 * @link			  https://www.semantica.co.za/
 * @since			  1.0.0
 * @package			  Semantica Core
 *
 * @wordpress-plugin
 * Plugin Name:		  Semantica Core
 * Plugin URI:		  https://www.semantica.co.za/
 * Description:		  Semantica Core Integrations
 * Version:			  1.0.0
 * Author:			  Semantica
 * Author URI:		  https://www.semantica.co.za/
 * License:			  GPL-2.0+
 * License URI:		  http://www.gnu.org/licenses/gpl-2.0.txt
 */

define('SEM_PATH', plugin_dir_path(__FILE__));

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Enque Scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
function sem_required_scripts()
{
	wp_register_style('sem_css', plugins_url('css/semantica-core.css', __FILE__));
	wp_enqueue_style('sem_css');
	wp_register_script('sem_jquery', plugins_url('js/semantica-core.js', __FILE__), array('jquery'), '', true);
	wp_enqueue_script('sem_jquery');
	wp_register_script('sem_slick', plugins_url('js/slick.js', __FILE__), array('jquery'), '', true);
	wp_enqueue_script('sem_slick');
	wp_register_style('sem_slick_css', plugins_url('css/slick.css', __FILE__));
	wp_enqueue_style('sem_slick_css');
	wp_register_style('sem_slick_theme_css', plugins_url('css/slick-theme.css', __FILE__));
	wp_enqueue_style('sem_slick_theme_css');
	wp_localize_script('sem_jquery', 'asc_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'sem_required_scripts');

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Includes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
require_once(SEM_PATH . '/kerridge/api.php');
require_once(SEM_PATH . '/shortcodes.php');

//Create custom meta box in order details page//
add_action('add_meta_boxes', 'add_shop_order_meta_box');
function add_shop_order_meta_box()
{
	add_meta_box(
		'k8_response',
		__('K8 Response', 'semantica_core'),
		'shop_order_display_callback',
		'shop_order',
		'side'
	);
}

//For displaying custom meta box in order details page//
function shop_order_display_callback($post)
{
	$value = get_post_meta($post->ID, 'k8_response', true);
	echo esc_attr($value);
}

//Send order information when a new order is placed// 
//add_action( 'woocommerce_new_order','action_woocommerce_new_order'); 
add_action('woocommerce_order_status_processing', 'action_woocommerce_new_order'); //Added to test sending of order without having to create a new order//
add_action('woocommerce_order_status_on-hold', 'action_woocommerce_new_order'); //Added to test sending of order without having to create a new order//

function action_woocommerce_new_order($order_id)
{

	$order = new WC_Order($order_id); //Get the new order//
	$id = $order->post->ID; //Get the order ID//
	$user_id = $order->user_id; //Set the user ID//

	//Set the address fields//
	$address_fields = array(
		'country',
		'title',
		'first_name',
		'last_name',
		'company',
		'address_1',
		'address_2',
		'address_3',
		'address_4',
		'city',
		'state',
		'postcode'
	);

	//Set the shipping and billing//
	$address = array();
	if (is_array($address_fields)) {
		foreach ($address_fields as $field) {
			//$address['billing_'.$field] = get_user_meta( $user_id, 'billing_'.$field, true );
			$address['shipping_' . $field] = get_user_meta($user_id, 'shipping_' . $field, true);
		}
	}

	$date_created = $order->get_date_created(); //Get order date//
	$delivery_address_line = $order->get_shipping_address_1() . ', ' . $order->get_shipping_address_2() . ', ' . $order->get_shipping_city() . ', ' . $order->get_shipping_state() . ', ' . $order->get_shipping_postcode(); //Set the delivery address line//
	$postcode = $order->get_shipping_postcode(); //Set the postcode//

	$k8_data = [
		"header" => [
			"account" => "ABI001",
			"branch" => "1",
			"createuser" => $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name(),
			"reference" => "Customer order number", //"Customer order number"//
			"reference2" => $id, //"reference2"//
			"daterequired" => $date_created->date("Y-m-d"), //"01-02-1990"//
			"name" => $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name(), //"Delivery name"//
			"address" => [
				"line" => [
					$delivery_address_line
				],
				"postcode" => $order->get_shipping_postcode() //"Postcode"//
			],
			"deliverycountry" => "Delivery country", //Delivery country"//
			"invoicename" => "Invoice name", //"Invoice name"//
			"invoiceaddress" => [
				"line" => [
					$delivery_address_line //"Invoice address line"//
				],
				"postcode" => $order->get_shipping_postcode() //"Postcode"//
			],
			"invoicecountry" => $order->get_shipping_country(), //"Invoice country"//
			"instructions" => $order->get_customer_note(), //"External text"//
			"narrative" => "Internal text", //"Internal text"//
			"ordertype" => "sale", //"sale"//
			"carrier" => "TNT", //"TNT"//
			"servicelevel" => "AM", //"AM"//
			"calccarriage" => "TRUE", //"TRUE"//
			"suspendorder" => "TRUE", //"TRUE"//
			"id" => "Suspend user", //"Suspend user"//
			"suspendmsg" => "Suspend message", //"Suspend message"//
			"ordercategory" => "C", //"C"//
			"deliverymethod" => "E", //"E"//
			"contactid" => "0", //"0"//
			"sourceofsale" => "Internet", //"Internet"//
			"contactphone" => $order->billing_phone, //"Telephone number"//
			"contactmobile" => $order->billing_phone, //"Mobile number"//
			"contactemail" => $order->billing_email, //"Email address"//
			"payments" => [
				"payment" => [
					[
						"type" => "E", //"E"//
						"value" => $order->order_total, //"100"//
						"reference" => "Payment reference" //"Payment reference"//
					]
				]
			],
			"lines" => [
				"line" => [
					[
						"product" => "0102097", //"0102097"//
						"manufact" => "", //""//
						"extendeddescription" => "WN PROF WATER COLOUR S4 (097) CADMIUM RED DEEP 5ML", //"WN PROF WATER COLOUR S4 (097) CADMIUM RED DEEP 5ML"//
						"quantity" => "1", //"1"//
						"unit" => "EA", //"EA"//
						"daterequired" => "2019-10-18", //"2019-10-18"//
						"specialinstructions" => "External text", //"External text"//
						"internalinstructions" => "Internal text", //"Internal text"//
						"price" => "100", //"100"//
						"priceunit" => "EA" //"EA"//
					]
				]
			]
		]
	];

	// send API request via cURL
	$ch = curl_init();
	// set the complete URL to process the order on the external system. Let’s consider https://example.com/placeorder.php is the URL, which invokes the API //
	curl_setopt($ch, CURLOPT_URL, 'https://xc7yq5c7jluyl47rqmay.kerridgecs.co.za/XI/Core/PlaceOrder');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($k8_data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	$response = curl_exec($ch);

	curl_close($ch);

	$arr = json_decode($response, true);

	if (!empty(response)) :
		update_post_meta($order_id, 'k8_response', $arr["body"]["orderNo"]);
	endif;
}
// End sending order information

//Make Kerridge API Calls
function kerridge_api()
{

	$apiCall = new apiCall();

	$request = array(
		$_REQUEST['api_acc_num'] => 'account'
	);

	if ($_REQUEST['api_method'] == 'GetBranches') {
		$request = array(
			'' => 'branch'
		);
	}

	$xml = new SimpleXMLElement('<root/>');
	array_walk_recursive($request, array($xml, 'addChild'));
	$body = $xml->asXML();

	//echo '<pre>'.print_r($body, true).'</pre>';
	if ($_REQUEST['api_method'] == 'GetBranches') {
		$body = '
		<root>
			<branch></branch>
		</root>
		';
	}

	if ($_REQUEST['api_method'] == 'GetAccountHistory') {
		$body = '
		<root>
			<account>' . $_REQUEST['api_acc_num'] . '</account>
			<filter>
				<year>2020</year>
				<period>12</period>
				<datefrom>2020-01-01</datefrom>
				<dateto>2020-12-30</dateto>
				<maxrecs>9000</maxrecs>
			</filter>
		</root>
		';
	}

	if ($_REQUEST['api_method'] == 'GetInvoices') {
		$body = '
		<root>
			<account>' . $_REQUEST['api_acc_num'] . '</account>
		</root>
		';
	}

	$response = $apiCall->GetAccountDetails($_REQUEST['api_method'], $body);

	//$_REQUEST['api-method']
	echo '<pre>' . print_r($response, true) . '</pre>';

	die();
}

add_action('wp_ajax_kerridge_api', 'kerridge_api');
add_action('wp_ajax_nopriv_kerridge_api', 'kerridge_api');